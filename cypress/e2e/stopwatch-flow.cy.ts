describe("Stopwatch flow", () => {
    it("loads the app and finds the correct heading", () => {
        cy.visit("/")
        cy.get('[data-cy="heading"]').contains("Stopwatch")
    })

    it("shows the timer as 00:00:00 initially", () => {
        cy.visit("/")
        cy.get('[data-cy="timer"]').contains("00:00:00")
    })

    it("shows the word 'Start' on the button initially", () => {
        cy.visit("/")
        cy.get('[data-cy="button"]').contains("Start")
    })

    it("shows the word 'Stop' on the button, after starting the timer", () => {
        cy.visit("/")
        const button = cy.get('[data-cy="button"]')
        button.click()
        button.contains("Stop")
    })

    it("should now not say '00:00:00' after starting timer", () => {
        cy.visit("/")
        cy.get('[data-cy="button"]').click()
        cy.get('[data-cy="timer"]').should("not.contain", "00:00:00")
    })

    it("resets after starting the timer a 2nd time", () => {
        cy.visit("/")
        const button = cy.get('[data-cy="button"]')
        // Click to start timer
        button.click()
        cy.wait(5000)
        // Click to stop timer
        button.click()
        // Click to start timer for the 2nd time
        button.click()
        cy.get('[data-cy="ss"]').contains("00")
    })

    it("says timer seconds are '10', after starting timer and waiting 10 seconds", () => {
        cy.visit("/")
        const button = cy.get('[data-cy="button"]')
        button.click()
        cy.wait(10500)
        button.click()
        cy.get('[data-cy="ss"]').contains("10")
    })

    it("says timer minutes are '01', after starting timer and waiting 1 minute'", () => {
        cy.visit("/")
        const button = cy.get('[data-cy="button"]')
        button.click()
        cy.wait(65000)
        button.click()
        cy.get('[data-cy="mm"]').contains("01")
    })
})
