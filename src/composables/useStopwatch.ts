import { ref, computed } from "vue"
import { useI18nStore } from "@/stores/i18n"

export function useStopwatch() {
    // Put translations in a `t` object
    const t = useI18nStore()

    const mm = ref<number>(0)
    const ss = ref<number>(0)
    const hh = ref<number>(0)

    let timer: ReturnType<typeof setInterval> = setInterval(() => {})
    const interval = 10

    const stopwatchIsRunning = ref<boolean>(false)

    const buttonText = computed<string>(() => {
        return stopwatchIsRunning.value
            ? t.button.textWhenStarted
            : t.button.textWhenStopped
    })

    function clickButton() {
        // Toggle wheter stopwatch is running or not
        stopwatchIsRunning.value = !stopwatchIsRunning.value

        if (stopwatchIsRunning.value) {
            resetStopWatch()
            timer = setInterval(() => {
                startStopwatch()
            }, interval)
        } else {
            stopStopwatch()
        }
    }

    function startStopwatch() {
        if (hh.value < 99) {
            hh.value++
        } else {
            hh.value = 0
            if (ss.value < 59) {
                ss.value++
            } else {
                ss.value = 0
                mm.value++
            }
        }
    }

    function stopStopwatch() {
        clearInterval(timer)
    }

    function resetStopWatch() {
        ;[mm.value, ss.value, hh.value] = [0, 0, 0]
    }
    return {
        mm,
        ss,
        hh,
        timer,
        interval,
        stopwatchIsRunning,
        buttonText,
        clickButton,
        startStopwatch,
        stopStopwatch,
        resetStopWatch,
    }
}
