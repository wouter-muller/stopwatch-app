import { defineStore } from "pinia"

export const useHelperStore = defineStore("helper", {
    actions: {
        addLeadingZero(value: number) {
            return value > 9 ? value : "0" + value
        },
    },
})
