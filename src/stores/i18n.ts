import { defineStore } from "pinia"

export const useI18nStore = defineStore("i18n", {
    state: () => {
        return {
            heading: "Stopwatch",
            button: {
                textWhenStopped: "Start",
                textWhenStarted: "Stop",
            },
        }
    },
})
